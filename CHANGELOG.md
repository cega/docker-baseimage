# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.0] - 2016-02-23
### Added
- `curl`, `git` and `openssh-client` for pulling in remote content inside containers built on this one.
- pre-loaded SSH host identities for github and bitbucket to allow non-interactive code pulls.
- `gosu` for easy user switching in docker.
- `supervisord` to provide proper process supervision (restart on fail, and reap zombies) and log redirection (via stdout, the Docker way).
- `en_AU` locale by default.
- `VOLUME ["/var/local"]` to provide a common data storage location to simplify volume linking.
- a /usr/local/sbin/docker-cleanup.sh script to conveniently clean up layered container builds in a constent way.
