# VRT Systems Docker base image

VRT Systems common docker base image for deriving other VRT docker images.

Derived from the official Docker Debian Jessie, with:

* `curl`, `git` and `openssh-client` for pulling in remote content inside
  containers built on this one.
* pre-loaded SSH host identities for github and bitbucket to allow non-
  interactive code pulls.
* `gosu` for easy user switching in docker.
* `supervisord` to provide proper process supervision (restart on fail, and
  reap zombies) and log redirection (via stdout, the Docker way).
* `en_AU` locale by default.
* `VOLUME ["/var/local"]` to provide a common data storage location to simplify
  volume linking.
* a /usr/local/sbin/docker-cleanup.sh script to conveniently clean up layered
  container builds in a constent way.

## How to use this image

Derive your docker image from this one (hosted at docker registry as vrtsystems/baseimage):

	FROM vrtsystems/baseimage

Then copy in a supervisord.conf file and start supervisord via ENTRYPOINT and/or CMD:

	COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
	CMD ["/usr/bin/supervisord"]

A sample supervisord.conf:

	[supervisord]
	logfile=/dev/null
	pidfile=/var/run/supervisord.pid
	nodaemon=true
	
	[program:postgres]
	command=/usr/lib/postgresql/9.4/bin/postgres -D /var/lib/postgresql/data
	user=postgres
	redirect_stderr=true
	stdout_logfile=/dev/stdout
	stdout_logfile_maxbytes=0
	stderr_logfile=/dev/stderr
	stderr_logfile_maxbytes=0
	auto_start=true
	autorestart=true

Note that setting stdout and stderr as shown above ensures that log output from supervisord child processes are passed to stdout so that "docker logs" works as it should.

For an example of this in action, see the [vrtsystems/postgres image](https://registry.hub.docker.com/u/vrtsystems/postgres/) on [bitbucket](https://bitbucket.org/vrtsystems/docker-postgres).

## References

* [Docker Traps and How to Avoid them](http://mrbluecoat.blogspot.com.au/2014/10/docker-traps-and-how-to-avoid-them.html)
* [Docker continuous delivery part 1](http://contino.co.uk/use-docker-continuously-deliver-microservices-part-1/)
* [Docker continuous delivery part 2](http://contino.co.uk/use-docker-continuous-delivery-part-2/)
* [Multi process docker images done right](http://tech.paulcz.net/2014/12/multi-process-docker-images-done-right/)
* [Dockerfile Best Practices - take 2](http://crosbymichael.com/dockerfile-best-practices-take-2.html)

